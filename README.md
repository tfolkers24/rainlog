﻿### About

Rainlog utility program, ***rain***, is used to visualize your records of rainfall at your location.  Intended for www.rainlog.org users, it provides a way to create plots and tables of you historical rain data.

## Dependencies:

 - Required for building: 
	 - Gnu Scientific Math package: ***libgsl, gslcblas*** 
	 - ***cmake***
	 - The usual tools: ***gcc, make***, etc.
   
  
 - Required for Use: 
	 - 2D plotting package: ***xmgrace*** (sometimes just called '***grace***') There is a source tree for ***grace*** in this package if one isn't available with your OS. 
	 - Gnu plotting package:  ***Gnuplot***

 
## Downloading:

The package is available via ***git***.

git clone https://bitbucket.org/tfolkers24/rainlog.git

## Building:

This package uses ***cmake*** and builds and compiles and tested on ***Fedora 36***.  I'm sure it will work with other 'linux'  systems but I don't have access to many others to test.  I do know there are problems with CentOS-7, but I'm thinking that's due to missing parts or bad ***cmake*** installation on my part.

Once the the package has been downloaded:

**NOTE**: *I have made a few assumptions based on my experience in the Arizona Desert.  Things like the likely number of times it rains in a particular year.  I have set that to 128 days.  If you live in a particularly rainy area you may want to adjust that number up.  It will just make your RAIN_GLOBAL file bigger.  See the file ./src/rain.h for these defines.  I also limit the global memory to 32 years.  Again, adjust as needed.*

 - cd to the directory where you downloaded the package
 - Edit the ./src/local.h and change the values for ***Station Number & Station Name***
 - mkdir _build
 - cd _build
 - cmake ..
 - make
 - make install

The ***install*** portion, by defaults, will create a **bin** directory adjacent to the **_build** directory and install the two binaries there. You should run the binary from this **bin** directory.  There is no need to install this in the system bin directories, so no ***root*** privilege is required.  By default the rain program expects the ***csv*** and ***share*** directories to be located adjacent to the ***bin*** directory, so it's important to ***cd*** to the ***bin*** directory and run this program from there.

## Input (Ingesting) Your Data:

The package comes with a sample database called rainlog.csv.  This is from my house and you will probably want to replace this with you own data.  You can download your personal database from the rainlog web site here:

https://rainlog.org/data/download

Place the rainlog_user_xxxx.zip file in the package root directory and unzip it.  It will ask to overwrite the current files, so you should answer 'y'.   Then, ***cd*** to the ***csv*** directory and change the symbolic link to point to your file:

    ln -sf readings_of_gauge_xxxx_xxx_address_Gauge.csv rainlog.csv

Obviously, substituting the actual filename with yours.

Then **cd** up and over to the ***bin*** directory and ***ingest*** the database like this:

**./rain ingest [.\./csv/rainlog.csv]**

The ***rain*** program will create a local database called: **RAIN_GLOBAL**
Once you have 'ingested' the 'csv' file you no longer need to ingest again.  All other commands use this created database for all data presentation.

If you update the rainlog.org web site with new rain gauge readings, then occasionally, you are going to want to download another copy of your database.  Just unzip it on top of the old one and 'ingest' it again.  This will only 'add' additional records to the database.  Alternately, you can edit the ***rainlog.csv*** files and manually add more entries to the end of the file.  Just remember that each days entry is for the rain that fell the day before.

If for some reason you suspect the database got corrupted, then just delete the **RAIN_GLOBAL** and ***ingest*** again.

## Running:

First, you need to either have ***xmgrace*** in your path or define **XMGRACE** to point to your installed copy of the binary.

    For bash: export XMGRACE=/usr/local/grace/bin/xmgrace  (or wherever it is)
    
    For tcsh: setenv XMGRACE /usr/local/grace/bin/xmgrace

As mentioned above, you need to ***ingest*** your input database into ***rain*** for any of the following commands to produce useful results.  Once the data has been ingested, then all the following commands are available:

    ./rain (with no args)
    
    Usage: ./rain args
     
    Usage: ./rain ingest [file]				Ingest 'file'; rainlog.csv by defaul
    Usage: ./rain info [file]				Print a brief listing about the loaded database
    Usage: ./rain plot [lines] 				Display plot of yearly rain fall
    Usage: ./rain plot monsoon [lines]; 	Display plot of yearly monsoon rain fall
    Usage: ./rain plot months 				Display 3D plot of yearly rain
    Usage: ./rain plot events 				Display plot of rain events
    Usage: ./rain monsoon					List rainfall during the Monsoon months
    Usage: ./rain totals					Show total rainfall for all years
    Usage: ./rain months					Show a monthly recap of rainfall
    Usage: ./rain event [n] [all]			Show the top 10 or [n] rain events
    Usage: ./rain histogram [bins] [yyyy]; 	Show distribution of rain events
    Usage: ./rain predict					Predict anticipated rainfall for year based on past averages
    Usage: ./rain trace						List a recap of 'trace' events
    Usage: ./rain daymode					Set plotting to be in 'Day' mode (White Background)
    Usage: ./rain nightmode					Set plotting to be in 'Night' mode (Black Background)

A breakdown of the commands follows:

 
**./rain ingest [file]**

Ingest the ../csv/rainlog.csv (by default).  You may pass the name of a different file if you prefer.  The database input file that comes from rainlog.org, should look like this:

    reading_date,reading_hour,reading_minute,quality,rain_amount,snow_depth,snow_accumulation,remarks
    2012-01-01,7,0,Good,0.0000,,,
    2012-01-02,7,0,Good,0.0000,,,
    2012-01-03,7,0,Good,0.0000,,,
    2012-01-04,7,0,Good,0.0000,,,
    2012-01-05,7,0,Good,0.0000,,,
    2012-01-06,7,0,Good,0.0000,,,
    2012-01-07,7,0,Good,0.0000,,,
    2012-01-08,7,0,Good,0.0000,,,
    2012-01-09,7,0,Good,0.0000,,,
    ...
    
	Obviously, it doesn't rain at my place much...

<hr>

**./rain info [file]**

This command dumps out a detail info about the database:

    Today's Year:                      2022
    Total Records in Database:         3916
    First Year in Database:            2012
    First Date in Database:      2012-01-01
    Last  Date in Database:      2022-09-22
    Span Between Dates:                3917
    Rain  Events in Database:           485
    Trace Events in Database:            56
    Grand total of all rain events: 139.070"
    Average Per Rain Event:           0.287"
     
    Peak Rainfall:                    2.530"
    Peak Rainfall occured:       2012-07-05
     
    Monsoon Start:                      167
    Monsoon End:                        274
    Today's DOY:                        269
    We are in the Monsoon season
    Number of days into Monsoon:        102
    Number of days left in Monsoon:       5
    Current Monsoon's Total:         10.020"
    Current Monsoon's Rank:               3
     
    Current Year-To-Date Total:      11.750"
    Current Year's Rank:                  8
<hr>

**./rain plot [lines]**

**./rain plot monsoon [lines]**

**./rain plot months**

**./rain plot events**

The 'plot' command will produce a few different type of plots. 

 1. Total rainfall for all years in the database. (default)
 2. Total rainfall for just the 'monsoon' portion of the years
 3. A 3D plot of the rainfall vs time of year.
 4. A 3D plot showing the magnitude of all rain event as impulses on the plot.

The first 2 forms take an additional argument of 'lines' to place a cross-hair on the plot indication todays sate.
<hr>

**./rain monsoon**

Prints a table of info regarding Monsoon rainfall.  **Note**: Monsoon season in Arizona is defined to be the 167th - 274th day of year. Edit the ./src/rain.h file if your area differs.

    Total Monsoon by year
    Year    Amount    Rank
    2012      9.60      4
    2013      3.95      8
    2014     10.58      2
    2015      6.11      7
    2016      6.91      6
    2017      3.85      9
    2018      9.06      5
    2019      2.68     10
    2020      1.76     11
    2021     12.27      1
    2022     10.02      3

<hr>

**./rain totals**

Prints a table of all yearly rainfalls:

    Total Rainfall by year
    Year   Amount   Rank
    2012    13.33     6
    2013    10.50     9
    2014    15.06     4
    2015    16.00     2
    2016    12.02     7
    2017     8.07    10
    2018    17.54     1
    2019    13.71     5
    2020     5.28    11
    2021    15.81     3
    2022    11.75     8

<hr>

**./rain months**

Prints out a table of rain for each month of each year and the totals associated with each.

**Note**:  When run in a terminal, there are various color highlights that don't show here.

     Year     Jan    Feb    Mar    Apr    May    Jun    Jul    Aug    Sep    Oct    Nov    Dec     Totals    Diff
    --------------------------------------------------------------------------------------------------------------
    2012:  |  0.52   0.31   0.80   0.42   0.02   0.12   6.54   2.20   0.74   0.01   0.31   1.34  |  13.33  |  0.60
    2013:  |  1.24   1.24   0.13   0.16   0.00   0.00   1.91   1.32   0.72   0.00   2.86   0.92  |  10.50  | -2.23
    2014:  |  0.00   0.03   0.49   0.04   0.00   0.00   4.23   2.75   3.60   1.63   0.00   2.29  |  15.06  |  2.33
    2015:  |  2.55   0.32   0.36   0.42   0.57   0.61   0.33   3.49   1.89   4.17   0.54   0.75  |  16.00  |  3.27
    2016:  |  2.07   0.20   0.02   0.64   0.00   1.90   0.93   2.74   1.34   0.00   0.24   1.94  |  12.02  | -0.71
    2017:  |  3.22   0.30   0.00   0.00   0.28   0.00   2.76   0.90   0.19   0.20   0.00   0.22  |   8.07  | -4.66
    2018:  |  0.83   1.97   0.07   0.00   0.01   0.70   3.36   3.27   1.73   3.89   0.39   1.32  |  17.54  |  4.81
    2019:  |  1.27   3.67   0.76   0.11   0.20   0.00   1.19   0.27   1.22   0.00   3.04   1.98  |  13.71  |  0.98
    2020:  |  0.59   0.90   1.29   0.04   0.04   0.01   0.41   1.33   0.02   0.02   0.38   0.25  |   5.28  | -7.45
    2021:  |  1.06   0.00   0.64   0.17   0.00   0.05   6.10   4.13   1.99   0.00   0.04   1.63  |  15.81  |  3.08
    2022:  |  0.98   0.36   0.39   0.00   0.00   0.30   3.58   5.06   1.08   0.00   0.00   0.00  |  11.75  | -0.98
    --------------------------------------------------------------------------------------------------------------
    Totals | 14.33   9.30   4.95   2.00   1.12   3.69  31.34  27.46  14.52   9.92   7.80  12.64  | 139.07
    Avgs:  |  1.30   0.85   0.45   0.18   0.10   0.34   2.85   2.50   1.32   0.99   0.78   1.26  |  12.73*
    --------------------------------------------------------------------------------------------------------------
    *NOTE: Grand yearly average does not include rainfal from this year; Grand total does
           Indicates wettest month in a perticular year
           Indicates ABOVE average rainfall for year
           Indicates BELOW average rainfall for year
    26 Sep 22 09:47:03.965 Number of Rain Events:    485
    26 Sep 22 09:47:03.965 Average Rain Per Event: 0.287"

<hr>

**./rain event [n] [all]**

Prints the top 10, 'n' or all,  rain events:

    Top 10 Rainfall Events
    Rank   Date     Amount
       1: 2012-07-05:  2.53
       2: 2014-09-09:  2.50
       3: 2013-11-23:  2.30
       4: 2015-08-08:  1.75
       5: 2015-10-07:  1.62
       6: 2016-06-30:  1.60
       7: 2021-07-23:  1.55
       8: 2015-01-31:  1.50
       9: 2018-09-20:  1.50
      10: 2018-10-14:  1.43

<hr>

**./rain histogram [bins] [yyyy]**

Plot a histogram of the volume of rain events.  You may pass a different number of bins to increase or decrease the individual bin sizes.  Also, you may pass a single year that you are interested in.
<hr>

**./rain predict**

Predict, based on how much rain you have received so far in the year, and add in the average amount of rain that can be expected from this day forward.

    Current Year-To-Date Total:                11.750"
    11 Year Average Rainfall by Today's Date:  10.787"
    10 Year Average Rainfall for Whole Year:   13.854"
    Additional Amount Expected this year:       3.067"
    Expected Total for This Year:              14.817"
    Which Would Rank This Year 5th out of 11

<hr>

**./rain trace**

List all the trace events in the database.

    2012: 07-22 07-23 12-28 
    2013: 01-28 08-19 08-24 
    2014: 08-26 09-04 09-28 10-18 
    2015: 01-12 01-27 05-17 07-02 07-05 07-13 07-14 07-15 
          07-16 07-18 07-19 07-29 08-07 09-04 09-08 09-21 
    2016: 01-07 05-01 06-11 06-12 07-19 09-26 10-03 10-25 
          12-16 
    2017: 07-14 
    2018: 10-01 10-22 
    2019: 01-10 01-14 02-02 07-15 08-23 12-04 
    2020: 01-27 03-10 03-28 07-03 09-09 
    2021: 06-23 10-26 
    2022: 06-18 07-10 07-23 08-03 08-12 
    Total Trace Events: 56

<hr>

**./rain daymode**

Tells the 'rain' program to plot all future gnuplot based plots with a white background.

<hr>

**./rain nightmode**

Tells the 'rain' program to plot all future gnuplot based plots with a black background.

<hr>

## Contact:

Please feel free to contact me with issues, suggestions and bug fixes.  Especially in the area of building on different systems.  I'm new to ***cmake*** and I'm sure there are plenty of changes that could allow for building on other systems.

tfolkers@arizona.edu


> Written with [StackEdit](https://stackedit.io/).










