#ifndef LOCAL_H
#define LOCAL_H

// User definable values

#define STATION_NUMBER 6374
#define STATION_NAME  "201 W Blackstone Road"

// Used for the 3D plot of rain vs. time
#define PLOT_INTERPOLATION_X 9
#define PLOT_INTERPOLATION_Y 9

/* Undefine if you want plots to be max_value and not
   rounded to the next larger whole number */
//#define USE_CEIL_ROUNDING 1

/* If your location has a Monsoon/Rainy Season, define it here. */
#define HAS_MONSOON    1

/* Day of Year for START and END of Monsoon Season

   NOTE:
   If START and END are not contained completely inside
   a single year; i.e. START in one year and END in the 
   next, results are undefined; so don't do that unles
   you can cleverly changed the code to accomidate that.
 */
#ifdef HAS_MONSOON
#define MONSOON_START 167
#define MONSOON_END   274
#endif


#endif
