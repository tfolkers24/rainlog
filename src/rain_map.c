#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/time.h>

#include "rain.h"               /* Definition of global memory */
#include "proto.h"

extern struct RAIN_GLOBAL *RG;

int rain_map(iswrite)                              /* Map to global memory */
int iswrite;
{
  int fd = -1, sz, isrd, iswr;
  char zero = 1;
  void *map;
  char name[256];

  sz = 0;

  strcpy(name, "./RAIN_GLOBAL");

  if( access(name, F_OK))                             /* Try to open the file */
  {                                    /* access failed, attempt to create it */
    if(!iswrite)
    {
      log_msg("RAIN_MAP: Can't create file without write access");
      return(1);
    }

    if((fd = open( name, O_CREAT | O_RDWR, 0664 )) < 0)
    {
      log_msg("RAIN_MAP: Global File Creation Failed");
      return(1);
    }
    iswr = 1;                                /* 0664 means we can write to it */

                            /* fill new file with zeros with unix lseek trick */
    lseek(fd, sizeof(struct RAIN_GLOBAL)-1, SEEK_SET );
    zero = 0;
    write(fd, &zero, 1 );

  }
  else                                                 /* File already exists */
  {
    isrd = (access(name, R_OK) == 0);
    iswr = iswrite && (access(name, W_OK) == 0);

    if(iswr && isrd) 
      fd = open(name, O_RDWR, 0);
    else
    if(isrd)
      fd = open(name, O_RDONLY, 0);

    if(fd < 0)
    {
      log_msg("RAIN_MAP: Could Not Open File %s", name);
      return(1);
    }

    sz = lseek(fd, 0l, SEEK_END );

                                   /* The file ought to be the expected size. */
    if(sz > 0 && sz != sizeof(struct RAIN_GLOBAL))
    {
      char *pwd;

      if( !(pwd = getenv("PWD")))
	pwd = "no pwd";

      log_msg( "RAIN_MAP: %s (pwd %s) has %d bytes but expected %d", 
	name, pwd, sz, sizeof(struct RAIN_GLOBAL));

      log_msg( "RAIN_MAP: Exiting");

      exit(3);

      if(!iswrite)         /* Can't create a file if not allowed write access */
      {
        log_msg("RAIN_MAP: Abort: Called with no write access");

	return(1);
      }

            
      if(( fd = open( name, O_CREAT | O_TRUNC | O_RDWR, 0664 )) < 0)
      {
        log_msg("RAIN_MAP: File Creation Failed");

	return(1);
      }

      /* fill new file with zeros with unix lseek trick */
      lseek(fd, sizeof(struct RAIN_GLOBAL)-1, SEEK_SET );
      zero = 0;
      write(fd, &zero, 1 );
    }
  }

  /* Map to the file */

  map = mmap(0, sizeof(struct RAIN_GLOBAL),
                PROT_READ | (iswr?PROT_WRITE:0), MAP_SHARED, fd, 0);

  close(fd);

  if(map == NULL)
  {
    log_msg("RAIN_MAP: could not map to %s", name);
    perror("mmap");

    return(1);
  }

  /* Result is also saved in a static pointer */

  RG = (struct RAIN_GLOBAL *)map;

  return(0);
}
