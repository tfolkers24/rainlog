#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <libgen.h>

#include "local.h"

/* Given a filename, a parameter file, 
   set the date, address and turn on/off markers
 */
int main(argc, argv)
int   argc;
char *argv[];
{
  int   lineson=0;
  double hline, vline;
  FILE *fp;
  char  buf[512], fname[256];

  if(argc < 2)
  {
    fprintf(stderr, "Usage: %s param_filename [ lines hline vline ]\n", argv[0]);

    return(-1);
  }

  strcpy(fname, argv[1]);

  if(argc > 2)
  {
    if(!strcmp(argv[2], "lines"))
    {
      lineson = 1;

      if(argc == 5)
      {
        hline = atof(argv[3]);
        vline = atof(argv[4]);
      }
      else
      {
        fprintf(stderr, "Usage: %s param_filename [ lines hline vline ]\n", argv[0]);

        return(-1);
      }
    }
    else
    {
      fprintf(stderr, "Usage: %s param_filename [ lines hline vline ]\n", argv[0]);

      return(-1);
    }
  }

  if((fp = fopen(fname, "r")) == NULL)
  {
    fprintf(stderr, "%s: Unable to open param_file: %s\n", argv[0], fname);

    return(-1);
  }

  while(fgets(buf, 128, fp) != NULL)			/* Read in parameter file and substitute things */
  {
    if(strstr(buf, "sformat"))
    {
      printf("default sformat \"%%.8g\"\n");
      continue;
    }
    else
    if(strstr(buf, "Blackstone"))			/*     subtitle "201 W Blackstone Rd" */
    {
      printf("    subtitle \"%s\"\n", STATION_NAME);
      continue;
    }

    if(lineson)
    {
      if(!strncmp(buf, "    line off", 12))
      {
        printf("    line on\n");
      }
      else
      if(!strncmp(buf, "    line 1.0", 12))
      {
	if(strstr(fname, "monsoon"))
	{
          printf("    line %f, 0.0, %f, 15.0\n", vline, vline);
	}
	else
	{
          printf("    line %f, 0.0, %f, 20.0\n", vline, vline);
	}
      }
      else
      if(!strncmp(buf, "    line 2.0", 12))
      {
	if(strstr(fname, "monsoon"))
	{
          printf("    line 167.0, %f, 274.0, %f\n", hline, hline);
	}
	else
	{
          printf("    line 0.0, %f, 365.0, %f\n", hline, hline);
	}
      }
      else						/* Else, just print it out */
      {
        printf(buf);
      }
    }
    else
    {
      printf(buf);
    }
  }

  fclose(fp);

  return(0);
} 
