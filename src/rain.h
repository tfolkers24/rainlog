
#ifndef RAIN_H
#define RAIN_H

#include "local.h"

#define MAX_YEARS      32	/* Good until 2044, then what??? */
#define MAX_MONTHS     12
#define MAX_DAYS       31
#define MAX_RAIN_DAYS 128

#define MAX_RAIN_EVENTS (MAX_YEARS * MAX_RAIN_DAYS)

struct RAIN_EVENTS {
	int   year;
        float rain;
        char dateStr[16];
};


struct RAIN_TALLIES {
	int set;
	int year;
	int mrank;
	int trank;

	double total;
	double monsoon;
};


struct RAIN_DAY {
	char dateStr[16];
	int    doy;
	int    trace;
	double mjd;
	double rain;
	char   comment[256];
};


struct RAIN_MONTH {
	struct RAIN_DAY days[MAX_DAYS];
};


struct RAIN_YEAR {
	int    isleap;
	int    mrank;
	int    trank;
	int    pad;

	double total;
	double monsoon;

	struct RAIN_DAY leapday;

	struct RAIN_MONTH months[MAX_MONTHS];
};

struct MONTHS {
        double total[12];
};

struct RAIN_GLOBAL {
	int    initialized;
	int    rain_event_indx;
	int    traceEvents;
	int    records;
	int    first_year;
	int    comments;

	int    spare_ints[16];

	char   peakDate[64];

	char   spare_chars[512];

	double peakRain;
	double grand_total;
	double yearly_average;

	double site_total_monsoon;
	double site_avg_monsoon;

	double spare_doubles[16];

        double ytdAverage[366];
        double totals[MAX_YEARS][366];

	struct RAIN_DAY     firstDay;
	struct RAIN_DAY     lastDay;
	struct MONTHS       monthTotals[MAX_YEARS];
	struct RAIN_TALLIES tallies[MAX_YEARS];
	struct RAIN_YEAR    years[MAX_YEARS];
	struct RAIN_EVENTS  rain_events[MAX_RAIN_EVENTS];
};

#endif
