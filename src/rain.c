#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <math.h>

#include <gsl/gsl_histogram.h>

#include "rain.h"
#include "proto.h"

#define BLACK       0
#define RED         1
#define GREEN       2
#define YELLOW      3
#define BLUE        4
#define MAGENTA     5
#define CYAN        6
#define WHITE       7
#define RESERVE     8
#define RESET       9

#define MAX_COLORS 10

struct RAIN_GLOBAL *RG;

int display_w = 1920;
int display_h = 1080;
int interactiveSession = 0;

char *months[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
char *rankStr[] = { "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th", "th", 
		    "th", "th", "th", "th", "th", "th", "th", "th", "th", "th", "st", 
		    "nd", "rd", "th", "th", "th", "th", "th", "th", "th", "th", "th", 
		    "th", "th", "th", "th", "th", "th", "th", "th" };

char *days[] = {
        " 1st",
        " 2nd",
        " 3rd",
        " 4th",
        " 5th",
        " 6th",
        " 7th",
        " 8th",
        " 9th",
        "10th",
        "11th",
        "12th",
        "13th",
        "14th",
        "15th",
        "16th",
        "17th",
        "18th",
        "19th",
        "20th",
        "21st",
        "22nd",
        "23rd",
        "24th",
        "25th",
        "26th",
        "27th",
        "28th",
        "29th",
        "30th",
        "31st"
};

int numDaysPerMonth[] = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }; /* total 366 */


char tok[256][256];

static char daytab[2][13]={
    {0,31,28,31,30,31,30,31,31,30,31,30,31},
    {0,31,29,31,30,31,30,31,31,30,31,30,31}
};

char xmgrace[256];


/* deNewLineIt() Replace all newlines and returns with spaces */
int deNewLineIt(buf)
char *buf;
{
  char *cp;

  for(cp=buf;*cp;cp++)
  {
    if( *cp == '\n' || *cp == '\r')
    {
      *cp = ' ';
    }
  }

  return(0);
}

/* Return 0 if interactive terminal session */
int getInteractive()
{
  int log_how;
  pid_t tgrp, pgrp;

  char *cp;

  cp = getenv("TERM");

  if(cp)
  {
    pgrp = getpgrp();
    tgrp = tcgetpgrp(STDOUT_FILENO);

//    fprintf(stdout, "TERM = %s\n", cp);
//    fprintf(stdout, "getpgrp = %d\n", pgrp);
//    fprintf(stdout, "tcgetpgrp = %d\n", tgrp);

    if(pgrp == tgrp)
    {
      log_how = 0;
      interactiveSession = 1;
    }
    else
    {
      log_how = 1;
      interactiveSession = 0;
    }
  }
  else
  {
    log_how = 1;
  }

  return(log_how);
}

int smart_log_open(name, offset)
char *name;
int offset;
{
  int log_how;

  log_how = getInteractive() + offset;

  log_open(name, log_how);

//  log_msg("Smart Logging Set to: %d", log_how);
//  log_msg("interactiveSession:   %d", interactiveSession);

  return(log_how);
}

int get_tok(edit)
char *edit;
{
  int c = 0;
  char *token;

  bzero((char *)&tok, sizeof(tok));

  token = strtok(edit, "-,\r\n");
  while(token != (char *)NULL)
  {
    sprintf(tok[c++], "%s", token);
    token = strtok((char *)NULL, "-,\r\n");
  }

  return(c);
}



int setColor(c)
int c;
{
  static int oldColor = -1;

  if(c == oldColor)
  {
    return(0);
  }

  switch(c)
  {
    case BLACK:   fprintf(stdout, "%c[1;0m",     0x1b); break;
    case RED:     fprintf(stdout, "%c[1;31m",    0x1b); break;
    case GREEN:   fprintf(stdout, "%c[1;32;1m",  0x1b); break;
    case YELLOW:  fprintf(stdout, "%c[1;33;1m",  0x1b); break;
    case BLUE:    fprintf(stdout, "%c[1;34;1m",  0x1b); break;
    case MAGENTA: fprintf(stdout, "%c[1;35;1m",  0x1b); break;
    case CYAN:    fprintf(stdout, "%c[1;36;1m",  0x1b); break;
    case WHITE:   fprintf(stdout, "%c[1;37;40m", 0x1b); break;
    case RESERVE: fprintf(stdout, "%c[0m",     0x1b); break;
    case RESET:   fprintf(stdout, "%c[22;39;49m", 0x1b); break;
  }

  oldColor = c;

  return(0);
}


int isLeap(year)
int year;
{
  int leap;

  leap = (((year % 4) == 0) && ((year%100) != 0)) || ((year%400) == 0);

  return(leap);
}


/* day_of_year: set day of year from month & day */

int day_of_year(year, month, day, ly)
int year, month, day, *ly;
{
  int i, leap;

  leap = isLeap(year);

  for(i=1;i<month;i++)
  {
    day += daytab[leap][i];
  }

  *ly = leap;

  return(day);
}




/* Add the average rainfall for the remaining months to the 
   current years total */
int predict()
{
  int    i, year, yr, doy, rank=99;
  double sofar, expected, total;

  year = getYear(0);
  yr   = year - RG->first_year;

  sofar = RG->years[yr].total;
  log_msg("Current Year-To-Date Total:               %7.3f\"", sofar);

  doy  = getDoy(0);
  log_msg("%2d Year Average Rainfall by Today's Date: %7.3f\"", yr+1, RG->ytdAverage[doy]);

	/* Only report years for which we have full years of data */
  log_msg("%2d Year Average Rainfall for Whole Year:  %7.3f\"", yr,   RG->ytdAverage[365]);

  expected = RG->ytdAverage[365] - RG->ytdAverage[doy];
  log_msg("Additional Amount Expected this year:     %7.3f\"", expected);

  total = sofar + expected;
  log_msg("Expected Total for This Year:             %7.3f\"", total);

  for(i=0;i<yr;i++)
  {
    if(total > RG->tallies[i].total && RG->tallies[i].trank < rank)
    {
      rank = RG->tallies[i].trank;
//      log_msg("Setting rank to %2d for year: %4d", rank, RG->tallies[i].year);
    }
  }

  log_msg("Which Would Rank This Year %d%s out of %d", rank, rankStr[rank], yr+1);

  return(0);
}


int info()
{
  int year, yr, doy, span, missing;
#ifdef HAS_MONSOON
  int sinceStart, untilEnd;
#endif

  if(!RG->initialized)
  {
    log_msg("Global has not been initialized");
    exit(1);
  }

  showMonths(0); /* Go get some tallies */
  sortTotals();

  year = getYear(0);

  log_msg("Today's Year:                      %4d", year);
  if(isLeap(getYear(0)))
  {
    log_msg("This year is a Leap year");
  }

  log_msg("Total Records in Database:        %5d", RG->records);
  log_msg("First Date in Database:      %s", RG->firstDay.dateStr);
  log_msg("Last  Date in Database:      %s", RG->lastDay.dateStr);

  span = (int)round(RG->lastDay.mjd - RG->firstDay.mjd);
  log_msg("Span Between Dates:              %6d", span);

  missing = span - RG->records;
  if(missing > 1)						/* Can be off by 1 */
  {
    log_msg("Possible Missing Records:        %6d", missing);
  }

  log_msg("Rain  Events in Database:         %5d", RG->rain_event_indx);
  log_msg("Trace Events in Database:         %5d", RG->traceEvents);
  log_msg("Grand total of all rain events: %7.2f\"", RG->grand_total);
  log_msg("                                %7.2f\'", RG->grand_total / 12.0);

  log_msg(" ");
  log_msg("Average Per Rain Event:         %7.2f\"", RG->grand_total / RG->rain_event_indx);
  log_msg("Average Days Between Events:    %7.2f", (double)span / (double)RG->rain_event_indx);

  log_msg(" ");
  log_msg("Peak Rainfall:                  %7.2f\"", RG->peakRain);
  log_msg("Peak Rainfall occured:       %s", RG->peakDate);

#ifdef HAS_MONSOON
  log_msg(" ");
  log_msg("Monsoon Start:                     %4d", MONSOON_START);
  log_msg("Monsoon End:                       %4d", MONSOON_END);
#endif

  doy = getDoy(0);

  log_msg("Today's DOY:                       %4d", doy);

#ifdef HAS_MONSOON
  if(doy > MONSOON_START && doy < MONSOON_END)
  {
    log_msg("We are in the Monsoon season");

    sinceStart = doy - MONSOON_START;
    untilEnd   = MONSOON_END - doy;

    log_msg("Number of days into Monsoon:       %4d", sinceStart);
    log_msg("Number of days left in Monsoon:    %4d", untilEnd);
  }
#endif

  yr = year - RG->first_year;

#ifdef HAS_MONSOON
  log_msg("Current Monsoon's Total:        %7.2f\"", RG->years[yr].monsoon);
  log_msg("Current Monsoon's Rank:       %4d / %2d", RG->years[yr].mrank, yr+1);
#endif

  log_msg(" ");
  log_msg("Current Year-To-Date Total:     %7.2f\"", RG->years[yr].total);
  log_msg("Current Year's Rank:          %4d / %2d", RG->years[yr].trank, yr+1);

  return(0);
}


/* The rainlog.csv file only has '"' in it if there is a comment */
int checkForComments(p, buf)
struct RAIN_DAY *p;
char *buf;
{
  int n;
  char *cp;

  cp = strchr(buf, '"');

  if(cp)
  {
    n = strlen(cp);

    if(n < 256)
    {
      strcpy(p->comment, cp);
    }
    else
    {
      strncpy(p->comment, cp, n-1);
      p->comment[n-1] = '\0';

      printf("Found Long comment: %s: %s\n", p->dateStr, p->comment);
    }

    return(1);
  }

  return(0);
}


int ingest(argc, argv)
int argc;
char *argv[];
{
  int n, isleap, doy;
  int month, day, year, newevents=0;
  char *string, line[512], fname[256], save[512];
  struct RAIN_DAY *p;
  FILE *fp;

  if(argc > 2)
  {
    strcpy(fname, argv[2]);
  }
  else
  {
    strcpy(fname, "../csv/rainlog.csv");
  }

  fp = fopen(fname, "r");

  if(!fp)
  {
    fprintf(stderr, "Unable to open file: %s\n", fname);
    exit(1);
  }

  RG->records = 0;

  while((string = fgets(line, sizeof(line), fp)) != NULL)
  {
    if(line[0] == '#')
    {
      continue;
    }

    if(strstr(line, "reading_date"))
    {
      continue;
    }

    deNewLineIt(line);

    strcpy(save, line);

    n = get_tok(line);

    if(n > 5)
    {
      year  = atoi(tok[0]);

      if(RG->first_year == 0)
      {
        RG->first_year = year;
      }

      month = atoi(tok[1]);
      day   = atoi(tok[2]);

      doy = day_of_year(year, month, day, &isleap);

      RG->years[year-RG->first_year].isleap = isleap;

      if(isleap && (month == 2) && (day == 29))
      {
        p = &RG->years[year-RG->first_year].leapday;
        log_msg("Using a Leap Day for line: %s", save);
      }
      else
      {
        p = &RG->years[year-RG->first_year].months[month-1].days[day-1];
      }

      sprintf(p->dateStr, "%04d-%02d-%02d", year, month, day);

      p->mjd  = makeJulDate(month, day, year-1900, 0.0);

      if(!strcmp(tok[5], "Trace"))
      {
	if(p->trace == 0)
	{
	  p->trace = 1;
	}

        RG->traceEvents++;
      }
      else
      {
        p->rain = atof(tok[6]);
      }

      RG->comments += checkForComments(p, save);

      if(p->rain && !p->doy)
      {
        newevents++;
      }

      p->doy  = doy;

      if(RG->records == 0)	/* First one ?? */
      {
        bcopy((char *)p, (char *)&RG->firstDay, sizeof(RG->firstDay));
      }

      bcopy((char *)p, (char *)&RG->lastDay, sizeof(RG->lastDay));

      RG->records++;
    }
    else
    {
      deNewLineIt(save);
      log_msg("Got a strange number of tokens, %d, from line: %s", n, save);
    }
  }

  fclose(fp);

  log_msg("Loaded %d Records, %d new; %3d comments", RG->records, newevents, RG->comments);

  RG->initialized = 1;

  return(0);
}




int insertRainEvent(p)
struct RAIN_DAY *p;
{
  struct RAIN_EVENTS *p1;

  p1 = &RG->rain_events[RG->rain_event_indx];

  p1->rain = p->rain;
  strcpy(p1->dateStr, p->dateStr);

  p1->year = atoi(p->dateStr);

  RG->rain_event_indx++;

  if(RG->rain_event_indx >= MAX_RAIN_EVENTS)
  {
    fprintf(stderr, "Too many rain events: %d\n", RG->rain_event_indx);
    exit(1);
  }

  return(0);
}


int printTopTenEvents(argc, argv)
int argc;
char *argv[];
{
  int    i, n = 10, year;
  char   yearStr[16];
  struct RAIN_EVENTS *p;

#ifdef DO_TRACE
  int    all = 0;
  struct RAIN_DAYS   *r;
#endif

  year = getYear(0);

  sprintf(yearStr, "%4d", year);

  if(argc > 2)
  {
    if(!strncmp(argv[2], "all", 3))
    {
      n   = RG->rain_event_indx;
#ifdef DO_TRACE
      all = 1;
#endif
    }
    else
    {
      n = atoi(argv[2]);
 
      if(n < 1 || n > MAX_RAIN_EVENTS)
      {
        n = 10;
      }
    }
  }

  setColor(RESET);
  log_msg("Top %d Rainfall Events", n);
  log_msg("Rank   Date     Amount");

  p = &RG->rain_events[0];
  for(i=0;i<n;i++,p++)
  {
    if(strstr(p->dateStr, yearStr))
    {
      setColor(RED);
    }

    log_msg("%4d: %s: %5.2f", i+1, p->dateStr, p->rain);

    setColor(RESET);
  }

#ifdef DO_TRACE
  if(all)
  {
    r = &RG->rain_days[0];
    for(i=0;i<n;i++,r++)
    {
      if(r->trace)
      {
        log_msg("%4d: %s: Trace", i+1, r->dateStr);
      }
    }
  }
#endif

  return(0);
}

#define PLACE_WINDOW 1

int plotEventsHisto(argc, argv)
int argc;
char *argv[];
{
  int    i, n, bins = 64, year = 0, rt=0, userBin = 0;
  double min, max, rain, lower, upper;
  struct RAIN_EVENTS *p;
  FILE *fp, *fp2;
  gsl_histogram *h;

  if(argc > 2)
  {
    bins = atoi(argv[2]);
    userBin = 1;
  }

  if(argc > 3)
  {
    year = atoi(argv[3]);
  }

  if(bins >= RG->first_year)	/* Check to see if user passed year first */
  {
    year = bins;
    userBin = 0;
  }
  else
  if(bins > 256 || bins < 8)
  {
    log_msg("Bins out of range: 6 - 256");
    usage();
    exit(1);
  }

  min = 0.0;

  /* rain_events are sorted, most to least, so 0 is most */
  if(!year)
  {
    max = RG->rain_events[0].rain * 1.01;
  }
  else			/* Find year; first encountered will be most for that year */
  {
    for(i=0;i<RG->rain_event_indx;i++)
    {
      if(RG->rain_events[i].year == year)
      {
        max = RG->rain_events[i].rain * 1.01;
        break;
      }
    }
  }

  if(!userBin)
  {
    bins = (int)round(max / 0.1);
  }

  log_msg("Historgram: Using Min: %.2f, Max: %.2f, bins: %d", min, max, bins);

  fp = fopen("histo.dat", "w");
  if(!fp)
  {
    log_msg("Unable to open 'histo.dat'");
    return(0);
  }
  fp2 = fopen("histo.dem", "w");
  if(!fp)
  {
    log_msg("Unable to open 'histo.dem'");
    return(0);
  }

  /* Allocate the histogram */
  h = gsl_histogram_alloc(bins);

  /* Set its range */
  gsl_histogram_set_ranges_uniform (h, min, max);

  n = RG->rain_event_indx;
  p = &RG->rain_events[0];

  /* Populate the histogram */
  for(i=0;i<n;i++,p++)
  {
    if(year)					/* Is user only interested in a certain year ? */
    {
      if(year == p->year)
      {
        gsl_histogram_increment(h, p->rain);
	rt++;
      }
    }
    else
    {
      gsl_histogram_increment(h, p->rain);
      rt++;
    }
  }

  /* Now print out the results */

  fprintf(fp, "Range    Events\n");

  for(i=0;i<bins;i++)
  {
    gsl_histogram_get_range(h, i, &lower, &upper);
    rain = gsl_histogram_get(h, i);

    fprintf(fp, "%.2f-%.2f %.0f\n", lower, upper, rain);
  }

  gsl_histogram_free (h);

  fprintf(fp2, "reset\n");

  if(year)
  {
    fprintf(fp2, "set title \"Rain Event Distribution for Year: %4d\\n%d Total Events\"\n", year, rt);
  }
  else
  {
    fprintf(fp2, "set title \"Rain Event Distribution\\n%d Total Events\"\n", rt);
  }

  fprintf(fp2, "set auto x\n");
  fprintf(fp2, "set style data histogram\n");
  fprintf(fp2, "set style histogram cluster gap 1\n");
  fprintf(fp2, "set style fill solid border -1\n");
  fprintf(fp2, "set boxwidth 0.9\n");
  fprintf(fp2, "set xtic rotate by -45 scale 0\n");
  fprintf(fp2, "#\n");
#ifdef PLACE_WINDOW
  fprintf(fp2, "set terminal x11 enhanced position %d,%d size %d,%d font \"%s\"\n", 
			display_w / 4, 0 /*display_h / 4*/,
			display_w / 2, display_h / 2,
			"Courier,14");
#else
  fprintf(fp2, "set terminal x11 enhanced size %d,%d font \"%s\"\n", 
			display_w / 2, display_h / 2,
			"Courier,14");
#endif
  fprintf(fp2, "#\n");
  fprintf(fp2, "plot 'histo.dat' using 2:xtic(1) ti col\n");
  fprintf(fp2, "#\n");
  fprintf(fp2, "pause -1 \"<cr> to exit\"\n");
  fprintf(fp2, "#\n");

  fclose(fp);
  fclose(fp2);

  system("gnuplot \"histo.dem\"");

  return(0);
}


int plotEvents(wr)
int wr;
{
  int   yr, mo, doy, dy, nyears, ndays, year, leap;
  int   peakYear, peakMonth, peakDay;
  double max_rain = 0.0;
  FILE *fp;
  char fname[256], sysBuf[256];
  struct RAIN_DAY *p;

  if(wr)
  {
    sprintf(fname, "rain_events.dat");

    fp = fopen(fname, "w");

    if(!fp)
    {
      fprintf(stderr, "Unable to open file: %s\n", fname);
      exit(1);
    }
  }

  bzero((char *)RG->rain_events, sizeof(RG->rain_events));
  RG->rain_event_indx = 0;

  year = getYear(0);

  nyears = (year - RG->first_year) + 1;

  for(yr=0;yr<nyears;yr++)
  {
    doy  = 1;
    leap = isLeap(yr+RG->first_year);

    for(mo=0;mo<12;mo++)
    {
      ndays = daytab[leap][mo+1];

      for(dy=0;dy<ndays;dy++)
      {
        p = &RG->years[yr].months[mo].days[dy];

	if(wr)
	{
          fprintf(fp, "%4d %3d %6.3f\n", yr+RG->first_year, doy, p->rain);
	}

        if(p->rain > max_rain)
        {
          max_rain = p->rain;

	  peakYear  = yr;
	  peakMonth = mo;
	  peakDay   = dy;
        }

        if(p->rain > 0.0)
        {
          insertRainEvent(p);
        }

        doy++;
      }
    }

    if(wr)
    {
      fprintf(fp, "\n");
    }
  }

  if(wr)
  {
    fclose(fp);
  }

  /* sort by rainfall event */
  qsort(&RG->rain_events[0], RG->rain_event_indx, sizeof(struct RAIN_EVENTS), (__compar_fn_t)eventSort);

  p = &RG->years[peakYear].months[peakMonth].days[peakDay];

  log_msg("Peak Rainfall of %.3f, happened on %s", p->rain, p->dateStr);
  log_msg("There were a total of %d rain events; %d Trace events", RG->rain_event_indx, RG->traceEvents);

  RG->peakRain = p->rain;
  strcpy(RG->peakDate, p->dateStr);

  if(!wr)
  {
    return(0);
  }

  sprintf(fname, "rain_events.dem");
  if((fp = fopen(fname, "w")) == NULL)
  {
    fprintf(stderr, "Unable to open output file \"%s\"\n", fname);

    exit(1);
  }

  fprintf(fp, "reset\n");
  fprintf(fp, "set title \"Rain Fall Events\"\n");

  fprintf(fp, "set xlabel \"Year\"\n");
  fprintf(fp, "set ylabel \"Day of Year\"\n");
  fprintf(fp, "set zlabel \"Rain Fall (Inches)\"\n");

  fprintf(fp, "#\n");
  fprintf(fp, "set border 4095\n");
  fprintf(fp, "set hidden3d\n");
  fprintf(fp, "#\n");

  fprintf(fp, "set xrange [%.2f:%.2f]\n", (double)RG->first_year, (double)year);
  fprintf(fp, "set yrange [%.2f:%.2f]\n", 1.0, 366.0);
  fprintf(fp, "set zrange [%.2f:%.2f]\n", 0.0, max_rain);

  fprintf(fp, "set ticslevel 0.0\n");

  fprintf(fp, "set terminal x11 enhanced %d size %d, %d position 500, 0\n", 21, display_w / 2, display_h / 2 );

  fprintf(fp, "splot \"rain_events.dat\" every 1::0::1024 with lines\n");

  fprintf(fp, "pause -1 \"Hit return to exit\"\n");

  fprintf(fp, "quit\n");

  fclose(fp);

  sprintf(sysBuf, "gnuplot rain_events.dem");

  system(sysBuf);

  return(0);
}


int computeYTD()
{
  int yr, nyears, year, leap, doy;

  if(!RG->initialized)
  {
    fprintf(stderr, "Global has not been initialized\n");
    exit(1);
  }

  bzero((char *)&RG->ytdAverage[0], sizeof(RG->ytdAverage));

  year  = getYear(0);

  nyears = (year - RG->first_year) + 1;

  for(yr=0;yr<nyears;yr++)
  {
    leap = isLeap(yr+RG->first_year);

    for(doy=0;doy<366;doy++)
    {
      RG->ytdAverage[doy] += RG->totals[yr][doy];
    }

    if(!leap) /* Copy in the 366th day on non leap years */
    {
      RG->ytdAverage[365]       = RG->ytdAverage[364];
    }
  }

  for(doy=0;doy<366;doy++)
  {
    RG->ytdAverage[doy] /= (double)nyears;
  }

  return(0);
}


int plot(wr, argc, argv)
int wr, argc;
char *argv[];
{
  int    i, yr, mo, doy, dy, nyears, ndays, year, leap, 
	 monflag, mostart, mostop, daystart, doystart, doystop;
  int    lineson = 0;
  FILE  *fp;
  char   fname[256], sysBuf[256], *pfile;
  double total;
  struct RAIN_DAY *p;

  if(!RG->initialized)
  {
    log_msg("Global has not been initialized");
    exit(1);
  }

  monflag  =    0;
  mostart  =    0;
  mostop   =   12;
  doystart =    0;
  doystop  =  366;
  pfile    = "../share/rainfall_totals_parms";

  if(argc > 2)
  {
    if(!strncmp(argv[2], "events", 3))
    {
      plotEvents(1);
      return(0);
    }
    else
    if(!strncmp(argv[2], "months", 4))
    {
      plotMonths();
      return(0);
    }
#ifdef HAS_MONSOON
    else
    if(!strncmp(argv[2], "monsoon", 4))
    {
      pfile    = "../share/monsoon_totals_parms";

      if(argc > 3)
      {
        if(!strncmp(argv[3], "lines", 5))
        {
          log_msg("Turning Plot Lines On");
          lineson = 1;
        }
      }
    }
#endif
    else
    if(!strncmp(argv[2], "lines", 5))
    {
      log_msg("Turning Plot Lines On");
      lineson = 1;
    }
  }

  bzero((char *)&RG->totals[0][0], sizeof(RG->totals));

  year = getYear(0);

  nyears = (year - RG->first_year) + 1;

  for(yr=0;yr<nyears;yr++)
  {
    total = 0.0;
    doy   = doystart;
    leap = isLeap(yr+RG->first_year);

    if(wr == 1)
    {
      log_msg("Generating Year Plot for %d", yr + RG->first_year);

      sprintf(fname, "yearly_total.%04d.dat", yr + RG->first_year);

      fp = fopen(fname, "w");

      if(!fp)
      {
        log_msg("Unable to open file: %s", fname);
        exit(1);
      }
    }

    for(mo=mostart;mo<mostop;mo++)
    {
      ndays = daytab[leap][mo+1];

      if(monflag && mo == 5)
      {
        daystart = 12;
      }
      else
      {
	daystart = 0;
      }

      for(dy=daystart;dy<ndays;dy++)
      {
        p = &RG->years[yr].months[mo].days[dy];

	total          += p->rain;
	RG->totals[yr][doy] = total;

        if(wr == 1)
	{
          fprintf(fp, "%3d %6.3f\n", doy, total);
	}

        doy++;
      }
    }

    if(!leap) /* Copy in the 366th day on non leap years */
    {
      RG->totals[yr][365] = RG->totals[yr][364];
    }

    RG->years[yr].total = total;

    if(wr == 1)
    {
      fclose(fp);
      log_msg("Wrote %d days to file: %s", doy, fname);
    }
  }

  if(!wr)
  {
    return(0);
  }

  sprintf(fname, "totals.%04d-%04d.dat", RG->first_year, year);
  fp = fopen(fname, "w");

  if(!fp)
  {
    log_msg("Unable to open file: %s", fname);
    exit(1);
  }

  for(i=doystart;i<doystop;i++)
  {
    fprintf(fp, "%03d ", i);

    for(yr=0;yr<nyears;yr++)
    {
      fprintf(fp, "%6.3f ", RG->totals[yr][i]);
    }

    fprintf(fp, "%6.3f ", RG->ytdAverage[i]);

#ifdef HAS_MONSOON
    fprintf(fp, "%d\n", ((doystart == 0) && ((i == MONSOON_START) || (i == MONSOON_END))));
#else
    fprintf(fp, "\n");
#endif
  }

  fclose(fp);

  if(lineson)
  {
    sprintf(sysBuf, "./setdate %s lines %f %d > /tmp/tmp_params", pfile, RG->tallies[nyears-1].total, getDoy(0));
    log_msg(sysBuf);
    system(sysBuf);

    pfile = "/tmp/tmp_params"; 
  }

  sprintf(sysBuf, "%s -geometry 1380x1019 -autoscale none -p %s -nxy %s &", xmgrace, pfile, fname);
  system(sysBuf);

  return(0);
}


/* Sort  by  rainfall events */
int eventSort(p1, p2)
struct RAIN_EVENTS *p1, *p2;
{
  if(p1->rain > p2->rain)
  {
    return(-1);
  }
  else
  if(p1->rain < p2->rain)
  {
    return(1);
  }

  return(0);
}

#ifdef HAS_MONSOON
/* Sort total monsoon rainfall in decending order */
int monsoonSort(p1, p2)
struct RAIN_TALLIES *p1, *p2;
{
  if(p1->monsoon > p2->monsoon)
  {
    return(-1);
  }
  else
  if(p1->monsoon < p2->monsoon)
  {
    return(1);
  }

  return(0);
}
#endif


/* Sort total rainfall in decending order */
int totalSort(p1, p2)
struct RAIN_TALLIES *p1, *p2;
{
  if(p1->total > p2->total)
  {
    return(-1);
  }
  else
  if(p1->total < p2->total)
  {
    return(1);
  }

  return(0);
}


int yearSort(p1, p2)
struct RAIN_TALLIES *p1, *p2;
{
  if(p1->year < p2->year)
  {
    return(-1);
  }
  else
  if(p1->year > p2->year)
  {
    return(1);
  }

  return(0);
}
  


#ifdef HAS_MONSOON
int monsoon(wr)
int wr;
{
  int    start, yr, mo, leap, day, ndays, year, nyears;
  char  *cp;
  double total, site_total;

  if(!RG->initialized)
  {
    log_msg("Global has not been initialized");
    exit(1);
  }

  if(wr)
  {
    cp = getNewCtime(0);

    year = getYear(0);

    nyears = (year - RG->first_year) + 1;

    printf("%s \n", cp);
    printf("%s Monsoon by year\n", cp);
    printf("%s Year      Amount   Rank      %%\n", cp);

    for(yr=0;yr<nyears;yr++)
    {
      printf("%s ", cp);

      if(RG->years[yr].monsoon > RG->site_avg_monsoon)
      {
        setColor(GREEN);
      }
      else
      if(RG->years[yr].monsoon < RG->site_avg_monsoon)
      {
        setColor(YELLOW);
      }
      else
      {
        setColor(RESET);
      }

      printf("%d     %6.2f\"    %2d     %5.1f\n", 
		yr+RG->first_year, RG->years[yr].monsoon, RG->tallies[yr].mrank,
		(RG->years[yr].monsoon / RG->site_avg_monsoon) * 100.0);
      setColor(RESET);
    }

    printf("%s \n", cp);
    printf("%s Total:  %7.2f\"\n", cp, RG->site_total_monsoon);

    printf("%s ", cp);
    setColor(GREEN);
    printf("Average:%7.2f\"\n", RG->site_avg_monsoon);
    setColor(RESET);

    printf("%s \n", cp);

    return(0);
  }

  RG->site_total_monsoon = 0.0; 
  site_total             = 0.0;

  year = getYear(0);

  nyears = (year - RG->first_year) + 1;

  for(yr=0;yr<nyears;yr++)
  {
    total = 0.0;
    leap = isLeap(yr+RG->first_year);

	/* Bad!!! I assumed MONSOON_START and STOP; must fix */
    for(mo=5;mo<9;mo++)
    {
      ndays = daytab[leap][mo+1];


      if(mo == 5)
      {
        start = 12;
      }
      else
      {
	start = 0;
      }

      for(day=start;day<ndays;day++)
      {
        total += RG->years[yr].months[mo].days[day].rain;
      }
    }

    RG->years[yr].monsoon   = total;
    RG->site_total_monsoon += total;   	/* Total amount received */

    if(yr < (nyears-1))
    {
      site_total += total;		/* Total amount for averages; not counting this year */
    }
  }

  RG->site_avg_monsoon = site_total / (double)(nyears-1);

  log_msg("Total Monsoon Rain For This Site:       %7.2f\"", RG->site_total_monsoon);
  log_msg("Average Monsoon, Not Counting this Year %7.2f\"", RG->site_avg_monsoon);

  return(0);
}
#endif

int sortTotals()
{
  int year, yr, nyears;

  if(!RG->initialized)
  {
    log_msg("Global has not been initialized");
    exit(1);
  }

  year = getYear(0);

  nyears = (year - RG->first_year) + 1;

  bzero((char *)&RG->tallies, sizeof(RG->tallies));

  for(yr=0;yr<nyears;yr++)
  {
    RG->tallies[yr].set     = 1;
    RG->tallies[yr].year    = yr+RG->first_year;;
    RG->tallies[yr].total   = RG->years[yr].total;
#ifdef HAS_MONSOON
    RG->tallies[yr].monsoon = RG->years[yr].monsoon;
#endif
  }

  /* sort by total rainfall */
  qsort(&RG->tallies[0], nyears, sizeof(struct RAIN_TALLIES), (__compar_fn_t)totalSort);

  /* Tag this sort order */
  for(yr=0;yr<nyears;yr++)
  {
    RG->tallies[yr].trank = yr+1;
  }

#ifdef HAS_MONSOON
  /* sort by total monsoon rainfall */
  qsort(&RG->tallies[0], nyears, sizeof(struct RAIN_TALLIES), (__compar_fn_t)monsoonSort);
#endif

  /* Tag this sort order */
  for(yr=0;yr<nyears;yr++)
  {
    RG->tallies[yr].mrank = yr+1;
  }

    /* sort back in year order */
  qsort(&RG->tallies[0], nyears, sizeof(struct RAIN_TALLIES), (__compar_fn_t)yearSort);

  /* Copy ranks into main year structure */
  for(yr=0;yr<nyears;yr++)
  {
    RG->years[yr].mrank = RG->tallies[yr].mrank;
    RG->years[yr].trank = RG->tallies[yr].trank;
  }

  return(0);
}


int showTotals()
{
  int year, yr, nyears;
  char *cp;

  if(!RG->initialized)
  {
    log_msg("Global has not been initialized");
    exit(1);
  }

  showMonths(0); /* Go get some tallies */

  cp = getNewCtime(0);

  year = getYear(0);

  nyears = (year - RG->first_year) + 1;

  printf("%s \n", cp);
  printf("%s Rainfall by year\n", cp);
  printf("%s Year      Amount   Rank      %%\n", cp);

  for(yr=0;yr<nyears;yr++)
  {
    printf("%s ", cp);

    if(RG->years[yr].total > RG->yearly_average)
    {
      setColor(GREEN);
    }
    else
    if(RG->years[yr].total < RG->yearly_average)
    {
      setColor(YELLOW);
    }
    else
    {
      setColor(RESET);
    }
 
    printf("%d     %6.2f\"    %2d   %7.1f\n", 
			yr+RG->first_year, RG->years[yr].total, RG->tallies[yr].trank,
			(RG->years[yr].total / RG->yearly_average) * 100.0);

    setColor(RESET);
  }

  printf("%s\n", cp);
  printf("%s Total:  %7.2f\"\n", cp, RG->grand_total);

  printf("%s ", cp);
  setColor(GREEN);
  printf("Average:%7.2f\"\n", RG->yearly_average);
  setColor(RESET);

  printf("%s\n", cp);

  return(0);
}


int printHorizonalLine(n, term)
int n, term;
{
  int i;

  for(i=0;i<n;i++)
  {
    printf("-");
  }

  if(term)
  {
    printf("\n");
  }

  return(0);
}


int showMonths(wr)
int wr;
{
  int    mo, year, month, yr, nyears, peakMonth[MAX_YEARS];
  double mtotals[12], ytotals[MAX_YEARS], 
	 diff, peak = 0.0, gtotal = 0.0, deduct = 0.0;

  if(!RG->initialized)
  {
    log_msg("Global has not been initialized");
    exit(1);
  }

  bzero((char *)mtotals,   sizeof(mtotals));
  bzero((char *)ytotals,   sizeof(ytotals));
  bzero((char *)peakMonth, sizeof(peakMonth));

  year  = getYear(0);
  month = getMonth(0);	/* produces months = 1 - 12 */

  nyears = (year - RG->first_year) + 1;

  /* determine peak months for each year and make totals */
  for(yr=0;yr<nyears;yr++)
  {
    peak = 0.0;

    for(mo=0;mo<12;mo++)
    {
      if(RG->monthTotals[yr].total[mo] > peak)
      {
        peak = RG->monthTotals[yr].total[mo];
	peakMonth[yr] = mo;
      }

      ytotals[yr] += RG->monthTotals[yr].total[mo];
      mtotals[mo] += RG->monthTotals[yr].total[mo];
    }

    if(yr != nyears-1)  /* do not average in this year */
    {
      gtotal += ytotals[yr];
    }
    else
    {
      gtotal += ytotals[yr];

      deduct = ytotals[yr];
    }
  }

  RG->grand_total = gtotal;
  RG->yearly_average = (gtotal-deduct) / (double)(nyears-1);

  if(!wr)
  {
    return(0);
  }

  printf("\n");

  printf(" Year    ");
  for(mo=0;mo<12;mo++)
  {
    printf(" %s   ", months[mo]);
  }

  /* Print out monthly entries and yearly totals, 
     highlighting the peak month in each year */
  printf("  Totals    Diff\n");

  printHorizonalLine(110, 1);

  for(yr=0;yr<nyears;yr++)
  {
    peak = 0.0;
    printf("%4d:  |", yr+RG->first_year);

    for(mo=0;mo<12;mo++)
    {
      if(peakMonth[yr] == mo)
      {
        setColor(RED);
      }
      else
      if(RG->monthTotals[yr].total[mo] == 0.0)
      {
        setColor(BLUE);
      }

      printf(" %5.2f ", RG->monthTotals[yr].total[mo]);
      setColor(RESET);
    }

    printf(" | %6.2f  | ", ytotals[yr]);

    diff = ytotals[yr]-((gtotal-deduct)/(double)(nyears-1));

    if(diff > 0.0)
    {
      setColor(GREEN);
    }
    else
    {
      setColor(YELLOW);
    }

    printf("%5.2f\n", diff);

    setColor(RESET);
  }

  printHorizonalLine(110, 1);

  /* Print out monthly grand totals */
  printf("Totals |");
  for(mo=0;mo<12;mo++)
  {
    printf(" %5.2f ", mtotals[mo]);
  }

  printf(" | %6.2f\n", gtotal);

  /* Print out monthly averages */
  printf("Avgs:  |");
  for(mo=0;mo<12;mo++)
  {
    if(mo < month)		/* Don't divide by nyears for months that havn't happened */
    {
      setColor(RESET);
      printf(" %5.2f ", mtotals[mo] / (double)nyears);
    }
    else
    {
      setColor(MAGENTA);
      printf(" %5.2f ", mtotals[mo] / (double)(nyears-1));
      setColor(RESET);
    }
  }

  setColor(MAGENTA);
  printf(" | %6.2f*\n", RG->yearly_average);
  setColor(RESET);

  printHorizonalLine(110, 1);

  setColor(MAGENTA);
  printf("*NOTE: Grand yearly average does not include rainfal from this year; Grand total does\n");
  setColor(RED);
  printf("       Indicates wettest month in a perticular year\n");
  setColor(GREEN);
  printf("       Indicates ABOVE average rainfall for year\n");
  setColor(YELLOW);
  printf("       Indicates BELOW average rainfall for year\n");
  setColor(RESET);

  fflush(stdout);

  log_msg("Number of Rain Events:   %4d", RG->rain_event_indx);
  log_msg("Average Rain Per Event: %5.3f\"", gtotal / RG->rain_event_indx);

  return(0);
}


int plotMonths()
{
  int    mo, year, yr, nyears, peakMonth[MAX_YEARS];
  double mtotals[12], ytotals[MAX_YEARS], peak = 0.0, max_month = 0.0;
  char demname[256], datname[256], sysBuf[256];
  FILE *fp;

  if(!RG->initialized)
  {
    log_msg("Global has not been initialized");
    exit(1);
  }

  bzero((char *)mtotals,   sizeof(mtotals));
  bzero((char *)ytotals,   sizeof(ytotals));
  bzero((char *)peakMonth, sizeof(peakMonth));

  year  = getYear(0);

  nyears = (year - RG->first_year) + 1;

  /* determine peak months for each year and make totals */
  for(yr=0;yr<nyears;yr++)
  {
    peak = 0.0;

    for(mo=0;mo<12;mo++)
    {
      if(RG->monthTotals[yr].total[mo] > peak)
      {
        peak = RG->monthTotals[yr].total[mo];
	peakMonth[yr] = mo;
      }

      ytotals[yr] += RG->monthTotals[yr].total[mo];
      mtotals[mo] += RG->monthTotals[yr].total[mo];
    }
  }

  strcpy(datname, "monthly_totals.dat");

  fp = fopen(datname, "w");
  if(!fp)
  {
    log_msg("Error opening: %s", datname);

    return(1);
  }

  /* Print out monthly entries and yearly totals, 
     highlighting the peak month in each year */

  for(yr=0;yr<nyears;yr++)
  {
    for(mo=0;mo<12;mo++)
    {
      fprintf(fp, "%.1f %.1f %5.2f\n", (double)(yr+RG->first_year), (double)(mo+1), RG->monthTotals[yr].total[mo]);

      /* find max rainfall overall */
      if(RG->monthTotals[yr].total[mo] > max_month)
      {
	max_month = RG->monthTotals[yr].total[mo];
      }
    }

    fprintf(fp, "\n");
  }

  fclose(fp);

#ifdef USE_CEIL_ROUNDING
  max_month = ceil(max_month);
#endif

  strcpy(demname, "monthly_totals.dem");
  fp = fopen(demname, "w");
  if(!fp)
  {
    log_msg("Error opening: %s", demname);

    return(1);
  }

  fprintf(fp, "reset\n");
  fprintf(fp, "set border 4095\n");
  fprintf(fp, "set hidden3d\n");

  fprintf(fp, "set title \"%s\"\n", STATION_NAME);
  fprintf(fp, "set xlabel \"Year\"\n");
  fprintf(fp, "set ylabel \"Month\"\n");
  fprintf(fp, "set zlabel \"Monthly Total\"\n");

  fprintf(fp, "set key off\n");

  fprintf(fp, "set samples %d*12\n", nyears);
  fprintf(fp, "set isosamples %d,12\n", nyears);

  if(PLOT_INTERPOLATION_X > 1 || PLOT_INTERPOLATION_Y > 1)
  {
    fprintf(fp, "set pm3d at s interpolate %d,%d\n", PLOT_INTERPOLATION_X, PLOT_INTERPOLATION_Y);
  }
  else
  {
    fprintf(fp, "set pm3d at s\n");
  }

  fprintf(fp, "set yrange   [%.2f:%.2f]\n", 1.0, 12.0);
  fprintf(fp, "set zrange   [%.2f:%.2f]\n", 0.0, max_month);
  fprintf(fp, "set cbrange  [%.2f:%.2f]\n", 0.0, max_month);

  fprintf(fp, "set ticslevel 0.0\n");

  fprintf(fp, "set palette model RGB\n");
  fprintf(fp, "set palette defined ( 0 \"green\", 1 \"blue\", 2 \"red\", 3 \"orange\" )\n");

  fprintf(fp, "set view %.1f, %.1f, 1.25, 1.0\n", 0.0, 90.0);

  fprintf(fp, "set terminal x11 enhanced 22 size 900, 900\n");


//  fprintf(fp, "splot \"%s\" every 1::0::1024 with lines\n", datname);
  fprintf(fp, "splot \"%s\" with lines\n", datname);

  fprintf(fp, "pause -1 \"Hit return to exit\"\n");
  fprintf(fp, "quit\n");
  fclose(fp);

  sprintf(sysBuf, "gnuplot %s", demname);

  system(sysBuf);

  return(0);
}


int computeTotals()
{
  int mo, day, yr, n, year, nyears;
  double total;
  struct RAIN_DAY *p;

  if(!RG->initialized)
  {
    fprintf(stderr, "Global has not been initialized\n");
    exit(1);
  }

  year = getYear(0);

  nyears = (year - RG->first_year) + 1;

  bzero((char *)&RG->monthTotals[0], sizeof(RG->monthTotals));

  for(yr=0;yr<nyears;yr++)
  {
    for(mo=0;mo<12;mo++)
    {
      n     = 0;
      total = 0.0;

      for(day=0;day<31;day++)
      {
        p = &RG->years[yr].months[mo].days[day];

        if(p->rain > 0.0)
        {
          total += p->rain;
          n++;
        }
      }

      if(mo == 1 && RG->years[yr].isleap)
      {
        p = &RG->years[yr].leapday;	/* Add in any possible leap day rain */
        if(p->rain > 0.0)
        {
          total += p->rain;
          n++;
        }
      }

      RG->monthTotals[yr].total[mo] = total;
    }
  }

  log_msg("");

  return(0);
}


int showTrace()
{
  int mo, day, n = 0, j, yr, year, nyears;
  char bigBuf[1024], tbuf[256];
  struct RAIN_DAY *p;

  if(!RG->initialized)
  {
    fprintf(stderr, "Global has not been initialized\n");
    exit(1);
  }

  year = getYear(0);

  nyears = (year - RG->first_year) + 1;

  for(yr=0;yr<nyears;yr++)
  {
    sprintf(bigBuf, "%4d: ", yr+RG->first_year);

    j = 0;

    for(mo=0;mo<12;mo++)
    {
      for(day=0;day<31;day++)
      {
        p = &RG->years[yr].months[mo].days[day];

        if(p->trace)
        {
	  sprintf(tbuf, "%s ", p->dateStr+5);
	  strcat(bigBuf, tbuf);
	  n++;
	  j++;

	  if(j == 8)
          {
            log_msg("%s", bigBuf);
            sprintf(bigBuf, "      ");
	    j = 0;
          }
        }
      }
    }

    if(j)
    {
      log_msg("%s", bigBuf);
    }
  }

  log_msg("Total Trace Events: %d", n);

  log_msg("");

  return(0);
}


int showComments()
{
  int mo, day, n = 0, yr, year, nyears;
  struct RAIN_DAY *p;

  if(!RG->initialized)
  {
    fprintf(stderr, "Global has not been initialized\n");
    exit(1);
  }

  log_msg("");

  year = getYear(0);

  nyears = (year - RG->first_year) + 1;

  for(yr=0;yr<nyears;yr++)
  {
    for(mo=0;mo<12;mo++)
    {
      for(day=0;day<31;day++)
      {
        p = &RG->years[yr].months[mo].days[day];

        if(strlen(p->comment))
        {
	  log_msg("%s %s", p->dateStr, p->comment);
	  n++;
        }
      }
    }
  }

  log_msg("");

  log_msg("Total Comments: %d", n);

  log_msg("");

  return(0);
}




/* determine screen resolution; */
int findScreenResolution()
{
  FILE *fp;
  int width, height;
  char *string, line[256];

  fp = popen("xdpyinfo | grep -oP 'dimensions:\\s+\\K\\S+'", "r");
  if(!fp)
  {
    return(1);
  }

  while((string = fgets(line, sizeof(line), fp)) != NULL)
  {
    if(strlen(line) > 1)
    {
      sscanf(line, "%dx%d", &width, &height);
      break;
    }
  }

  pclose(fp);

  log_msg("Current screen: %d x %d", width, height);

  display_w = width;
  display_h = height;

  return(0);
}


int usage()
{
  log_msg("Usage: ./rain args");
  log_msg(" ");
  log_msg("Usage: ./rain ingest [file]\t\tIngest 'file'; rainlog.csv by defaul");
  log_msg("Usage: ./rain info \t\t\tPrint a brief listing about the loaded database");
  log_msg("Usage: ./rain comments \t\t\tPrint any comments in the loaded database");
  log_msg("Usage: ./rain plot [lines] \t\tDisplay plot of yearly rain fall");
#ifdef HAS_MONSOON
  log_msg("Usage: ./rain plot monsoon [lines]; \tDisplay plot of yearly monsoon rain fall");
#endif
  log_msg("Usage: ./rain plot months \t\tDisplay 3D plot of yearly rain");
  log_msg("Usage: ./rain plot events \t\tDisplay plot of rain events");
#ifdef HAS_MONSOON
  log_msg("Usage: ./rain monsoon\t\t\tList rainfall during the Monsoon months");
#endif
  log_msg("Usage: ./rain totals\t\t\tShow total rainfall for all years");
  log_msg("Usage: ./rain months\t\t\tShow a monthly recap of rainfall");
  log_msg("Usage: ./rain event [n] [all]\t\tShow the top 10 or [n] rain events");
  log_msg("Usage: ./rain histogram [bins] [yyyy]; \tShow distribution of rain events");
  log_msg("Usage: ./rain predict\t\t\tPredict anticipated rainfall for year based on past averages");
  log_msg("Usage: ./rain trace\t\t\tList a recap of 'trace' events");
  log_msg("Usage: ./rain daymode\t\t\tSet plotting to be in 'Day' mode (White Background)");
  log_msg("Usage: ./rain nightmode\t\t\tSet plotting to be in 'Night' mode (Black Background)");

  return(0);
}



int main(argc, argv)
int argc;
char *argv[];
{
  char *cp;

  smart_log_open("rain", 3);

  if(rain_map(1))
  {
    fprintf(stderr, "Unable to map Global\n");
    exit(1);
  }

  cp = getenv("XMGRACE");
  if(cp)
  {
    strcpy(xmgrace, cp);
  }
  else
  {
    strcpy(xmgrace, "/usr/local/grace/bin/xmgrace");
  }

  if(argc > 1)
  {
    if(!strncmp(argv[1], "ingest", strlen(argv[1])))
    {
      RG->traceEvents = 0;
      RG->comments    = 0;

      ingest(argc, argv);

      plot(0, 0, NULL);		/* Tally up totals without plotting */
#ifdef HAS_MONSOON

      if(MONSOON_END < MONSOON_START)
      {
        log_msg("MONSOON_END < MONSOON_START; don't expect Monsoon functions to work properly");
      }

      monsoon(0);		/* Tally up monsoons without printing */
#endif
      sortTotals();		/* Sort monsoon and totals by rank */
      computeTotals();
      computeYTD();
      plotEvents(0);		/* Fill in the event array and sort */

      return(0);
    }
    else
    if(!strncmp(argv[1], "plot", strlen(argv[1])))	/* totals, monsoons, months, events */
    {
      findScreenResolution();
      plot(2, argc, argv);

      return(0);
    }
#ifdef HAS_MONSOON
    else
    if(!strncmp(argv[1], "monsoon", strlen(argv[1])))	/* Print a table of just the monsoon months */
    {
      monsoon(1);

      return(0);
    }
#endif
    else
    if(!strncmp(argv[1], "info", strlen(argv[1])))	/* dump info about database */
    {
      info();

      return(0);
    }
    else
    if(!strncmp(argv[1], "totals", strlen(argv[1])))	/* Print a table of all years */
    {
      showTotals();

      return(0);
    }
    else
    if(!strncmp(argv[1], "months", strlen(argv[1])))	/* Print out table of months by year with totals */
    {
      showMonths(1);

      return(0);
    }
    else
    if(!strncmp(argv[1], "events", strlen(argv[1])))	/* List the top ten rainfall events, or n */
    {
      printTopTenEvents(argc, argv);

      return(0);
    }
    else
    if(!strncmp(argv[1], "histogram", strlen(argv[1])))	/* plot histogram of rainfall events */
    {
      findScreenResolution();
      plotEventsHisto(argc, argv);

      return(0);
    }
    else
    if(!strncmp(argv[1], "predict", strlen(argv[1])))	/* predict the total for the year based on average monthly values */
    {
      predict();

      return(0);
    }
    else
    if(!strncmp(argv[1], "trace", strlen(argv[1])))	/* Show all Trace Events */
    {
      showTrace();

      return(0);
    }
    else
    if(!strncmp(argv[1], "comments", strlen(argv[1])))	/* Show all Comments */
    {
      showComments();

      return(0);
    }
    else
    if(!strncmp(argv[1], "daymode", strlen(argv[1])))	/* Configure Day Mode Plotting*/
    {
      char sysBuf[256];

      log_msg("Setting Future Plotting to be in Day Mode");

      sprintf(sysBuf, "xrdb -merge ../share/daymode.Xdefaults");

      system(sysBuf);

      return(0);
    }
    else
    if(!strncmp(argv[1], "nightmode", strlen(argv[1])))	/* Configure Night Mode Plotting*/
    {
      char sysBuf[256];

      log_msg("Setting Future Plotting to be in Night Mode");

      sprintf(sysBuf, "xrdb -merge ../share/nightmode.Xdefaults");

      system(sysBuf);

      return(0);
    }
  }

  usage();

  return(1);
}
